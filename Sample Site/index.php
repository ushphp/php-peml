<?php $filepath = __FILE__; require(getenv("DOCUMENT_ROOT") . "/peml/pemlRender.php"); ?>
:doctype
html(lang:en)
  head
    title peml :: Sample Site
  
  body
    h1 Well done!
    h2 it looks like everything is working.
    
    hr
    
    p Check out <a href="http://erudit.es/peml/tutorial.php">the tutorial</a> for a quickstart to peml markup, or consult <a href="http://erudit.es/peml/tutorial.php">the reference</a> for a full overview of the core language.